## Signal-based downloader for the Tor browser

This is a fairly work-in-progress Signal bot used to request, and receive copies of the Tor browser.


This was directly inspired by [OnionSproutsBot](https://github.com/panos/OnionSproutsBot/), as well as the GetTor project.

---

What works:
 * [x] Listing the available platforms and languages.
 * [x] Ensuring that there is a timer always set, (erasing past history).
 * [x] Getting a copy of the Tor browser in any supported language, for any architecture (`win32/64`, `osx64`, `linux32/64`) along with its signature, even if it is not already on the disk.
 * [ ] Getting a copy of Android-based builds of the Tor browser (see TODOs).

TODOs:
 * Find a stable way of receiving a JSON-based feed of downloads.
 * Implement a system to retreive the Android builds of Tor.
 * Better documentation as well as l10n/i18n.
 * Time-based clean-up task to ensure that the builds are latest.

## Installation instructions:

Prior to running the bot, you must first have a properly-configured installation of [Signald](https://gitlab.com/signald/signald). 

Please follow their instructions, along with [Semaphore's](https://github.com/lwesterhof/semaphore) quick-start guide to get a Signal account on your Signald installation.

After having properly installed and configured Signald, clone this repository, and (preferably) create a Python virtual environment.

In the virtual environment, install the dependencies with `pip3 install -r requirements.txt`.

Copy `config.example.py` to `config.py`, and modify it to your liking, and run the bot with `python3 run.py`.